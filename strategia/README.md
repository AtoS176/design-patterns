  <h1>Wzorzec Strategia</h1>
  
  <p>
  Wzorzec, w którym istnieje wiele algorytmów (strategii wykonania) dla 
  konkretnego problemu i użytkownik sam w czasie wykonania może zadecydować,
   który z dostępnych algorytmów zastosować. 
  </p>
  
  <h4>Kiedy stosować : </h4>
  <ul>
  <li>Gdy chcesz używać różnych wariantów jednego algorytmu w obrębie obiektu i
   zyskać możliwość zmiany wyboru wariantu w trakcie działania programu.</li>
  <li>Strategia pozwala odizolować logikę biznesową klasy od szczegółów 
  implementacyjnych algorytmów, które nie są istotne w kontekście tej logiki.</li>
  </ul>
  
  <br>
  <p>Linki :
  <br>
  <a href="https://refactoring.guru/pl/design-patterns/strategy">Refactoring Guru</a>
  <br>
  <a href="https://www.baeldung.com/java-strategy-pattern">Beldung</a>
  <br>
  <a href="https://springframework.guru/gang-of-four-design-patterns/strategy-pattern/">Spring Framework Guru</a>
  </p> 