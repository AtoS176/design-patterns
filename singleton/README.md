<h1>Wzorzec Singleton</h1>

<p>
Singleton jest kreacyjnym wzorcem projektowym, który pozwala zapewnić
istnienie wyłącznie jednej instancji danej klasy. Ponadto daje globalny
punkt dostępowy do tejże instancji.
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li>Gdy w twoim programie ma prawo istnieć wyłącznie jeden ogólnodostępny obiekt 
danej klasy. Przykładem może być połączenie z bazą danych, którego używa wiele fragmentów programu.</li>
</ul>


<br>
<p>
Linki :
<br>
<a href="https://refactoring.guru/pl/design-patterns/singleton">Refactoring Guru</a>
</p>
