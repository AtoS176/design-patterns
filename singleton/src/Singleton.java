public final class Singleton {
    private static volatile Singleton instance;
    private String info;

    private Singleton(String info){
        this.info = info;
    };

    public static Singleton getInstance(String value){
        Singleton result = instance;
        if(result != null){
            return result;
        }
        synchronized (Singleton.class){
            if(instance == null){
                instance = new Singleton(value);
            }
            return instance;
        }
    }

    public String getInfo() {
        return info;
    }

}
