public class Main {
    public static void main (String [] args){
        System.out.println("If you see the same value, then singleton was reused." + "\n" +
                "If you see different values, then 2 singletons were created." + "\n\n" +
                "RESULT:" + "\n");

        Thread firstThread = new Thread(new FirstThread());
        Thread secondThread = new Thread(new SecondThread());
        firstThread.start();
        secondThread.start();
    }

    static class FirstThread implements Runnable{
        @Override
        public void run() {
            Singleton singleton = Singleton.getInstance("First");
            System.out.println(singleton.getInfo());
        }
    }

    static class SecondThread implements Runnable{
        @Override
        public void run() {
            Singleton singleton = Singleton.getInstance("Second");
            System.out.println(singleton.getInfo());
        }
    }

}
