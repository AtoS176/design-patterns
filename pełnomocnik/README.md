<h1>Wzorzec Pełnomocnik</h1>

<p>
Pełnomocnik to strukturalny wzorzec projektowy pozwalający stworzyć 
obiekt zastępczy w miejsce innego obiektu. Pełnomocnik nadzoruje 
dostęp do pierwotnego obiektu, pozwalając na wykonanie jakiejś czynności
 przed lub po przekazaniu do niego żądania. 
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li>
 Leniwa inicjalizacja <b> -> wirtualny pełnomocnik</b>. Gdy masz do czynienia 
z zasobożernym obiektem usługi, którego potrzebujesz jedynie co jakiś czas.
</li>
<li>
Kontrola dostępu <b> -> pełnomocnik ochronny</b>. Przydatne, gdy chcesz pozwolić
 tylko niektórym klientom na korzystanie z obiektu usługi. 
 Na przykład, gdy usługi stanowią kluczową część systemu operacyjnego,
 a klienci to różne uruchamiane aplikacje (również te szkodliwe).
</li>
<li>
Lokalne uruchamianie zdalnej usługi <b> -> pełnomocnik zdalny</b>.
 Użyteczne, gdy obiekt udostępniający usługę znajduje się na zdalnym serwerze.
</li>
<li>
Prowadzenie dziennika żądań <b> -> pełnomocnik prowadzący dziennik</b>. 
Pozwala prowadzić rejestr żądań przesyłanych do obiektu usługi.
</li>
<li>
Przechowywanie w pamięci podręcznej wyników działań 
<b> -> pełnomocnik z pamięcią podręczną</b>. Pozwala przechować wyniki
 przekazywanych żądań i zarządzać cyklem życia pamięci podręcznej. Szczególnie ważne przy dużych wielkościach danych wynikowych.
</li>
</ul>

<br>
<p>Linki :
<br>
<a href="https://refactoring.guru/pl/design-patterns/proxy">Refactoring Guru</a>
<br>
<a href="https://springframework.guru/gang-of-four-design-patterns/proxy-pattern/">Spring Framework Guru</a>
</p> 
