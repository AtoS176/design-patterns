<h1>Wzorzec Dekorator</h1>

<p>
Dekorator to strukturalny wzorzec projektowy pozwalający dodawać nowe obowiązki
obiektom poprzez umieszczenie tych obiektów w specjalnych obiektach opakowujących,
które zawierają odpowiednie zachowania.
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li>Gdy chcemy przypisać dodatkowe obowiązki obiektom w trakcie działania programu.</li>
<li>Kiedy rozszerzanie funkcjonalności poprzez podklasy przestaje być pratyczne.</li>
</ul>

<br>
<p>Linki :
<br>
<a href="https://refactoring.guru/design-patterns/decorator">Refactoring Guru</a>
<br>
<a href="https://www.journaldev.com/1540/decorator-design-pattern-in-java-example">JournalDev</a>
</p> 