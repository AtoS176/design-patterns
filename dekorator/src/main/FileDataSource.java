package main;

import java.io.*;

public class FileDataSource implements DataSource {
    private String name;

    public FileDataSource(String name){
        this.name = name;
    }

    @Override
    public void writeData(String data) {
        File file = new File(name);
        try{
            OutputStream fos = new FileOutputStream(file);
            fos.write(data.getBytes(), 0, data.length());
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public String readData() {
        StringBuilder text = new StringBuilder();
        File file = new File(name);
        try {
            String line;
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while((line = reader.readLine()) != null){
                text.append(line);
            }
        } catch (IOException e) {
            e.getMessage();
        }
        return text.toString();
    }

}
