package test;

import main.Movie;
import main.TvShow;

import java.util.List;

public class Runner {
    public static void main (String [] args) throws CloneNotSupportedException {
        Movie movie = new Movie("Black Forest", "Thriller", List.of("Thom", "Anna", "Lisa"));
        TvShow show = new TvShow("Warriors", "Action", List.of("First Fight", "Pride", "Furious"));

        System.out.println(movie.toString());
        System.out.println(show.toString());

        Movie movieCopy = movie.clone();
        TvShow showCopy = show.clone();

        System.out.println(movieCopy);
        System.out.println(showCopy);

    }
}
