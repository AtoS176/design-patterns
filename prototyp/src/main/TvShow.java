package main;

import java.util.List;

public class TvShow implements Prototype {
    private final String name;
    private final String type;
    private final List<String> episodes;

    public TvShow(String name, String type, List<String> episodes) {
        this.name = name;
        this.type = type;
        this.episodes = episodes;
    }

    @Override
    public TvShow clone() throws CloneNotSupportedException {
        System.out.println("Cloning tvShow ...");
        return (TvShow) super.clone();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        episodes.forEach(e -> stringBuilder.append(e).append("\n"));
        return "Name : " + name + "\n"
                + "Type : " + type + "\n"
                + "Episodes : " + "\n" + stringBuilder;
    }

}
