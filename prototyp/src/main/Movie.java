package main;

import java.util.List;

public class Movie implements Prototype{
    private final String name;
    private final String type;
    private final List<String> actors;

    public Movie(String name, String type, List<String> actors) {
        this.name = name;
        this.type = type;
        this.actors = actors;
    }

    @Override
    public Movie clone() throws CloneNotSupportedException {
        System.out.println("Cloning movie ...");
        return (Movie) super.clone();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        actors.forEach(a -> stringBuilder.append(a).append("\n"));
        return "Name : " + name + "\n"
                + "Type : " + type + "\n"
                + "Actors : " + "\n" + stringBuilder + "\n";
    }

}
