package main;

public interface Prototype extends Cloneable {
    Prototype clone() throws CloneNotSupportedException;
}
