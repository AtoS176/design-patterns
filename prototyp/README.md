<h1>Wzorzec Prototyp</h1>

<p>
Prototyp to kreacyjny wzorzec projektowy, który umożliwia kopiowanie już
istniejących obiektów bez tworzenia zależności pomiędzy twoim kodem,
a klasami obiektów.
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li> Gdy chcesz aby twój kod nie był zależny od konkretnej klasy kopiowanego obiektu.</li>
</ul>

<br>
<p>Linki :
<br>
<a href="https://howtodoinjava.com/design-patterns/creational/prototype-design-pattern-in-java/">How to do in Java</a>
<br>
<a href="https://springframework.guru/gang-of-four-design-patterns/prototype-pattern/">Spring Framework Guru</a>
<br>
<a href="https://pl.wikipedia.org/wiki/Prototyp_(wzorzec_projektowy)">Wikipedia</a>
</p> 
