<h1>Wzorce projektowe</h1>

<h4>Strukturalne</h4>

<p>Wzorce strukturalne wyjaśniają w jaki sposób można składać obiekty i klasy w większe struktury zachowując przy tym elastyczność i efektywność tych struktur.</p>

<ul>
<li>Adapter</li>
<li>Dekorator</li>
<li>Fasada</li>
<li>Kompozyt</li>
<li>Most</li>
<li>Pyłek</li>
<li>Pełnomocnik</li>
</ul>

