package test;

import main.*;

public class Runner {
    public static void main(String [] args){
        Runway runwayA = new Runway("RunwayA");

        AircraftMediator mediator = new ControlRoom();
        mediator.registerRunway(runwayA);

        AircraftColleague wrightFlight = new Aircraft(mediator, "Wright Flight");
        AircraftColleague airbusA380 = new Aircraft(mediator, "Airbus A380");

        wrightFlight.startLanding();
        airbusA380.startLanding();
        wrightFlight.finishLanding();
    }
}
