package main;

public interface AircraftMediator {
    void registerRunway(Runway runway);
    String allotRunwayTo(AircraftColleague aircraftColleague);
    void releaseRunwayOccupiedBy(AircraftColleague aircraftColleague);
}
