package main;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class ControlRoom implements AircraftMediator {
    private LinkedList<Runway> availableRunways = new LinkedList<>();
    private Map<AircraftColleague, Runway> aircraftRunwayMap = new HashMap<>();

    @Override
    public void registerRunway(Runway runway) {
        availableRunways.add(runway);
    }

    @Override
    public String allotRunwayTo(AircraftColleague aircraftColleague) {
        Runway nextAvailableRunway = null;
        if(!this.availableRunways.isEmpty()){
            nextAvailableRunway = availableRunways.removeFirst();
            aircraftRunwayMap.put(aircraftColleague, nextAvailableRunway);
        }
        return nextAvailableRunway == null ?
                null : nextAvailableRunway.getName();
    }

    @Override
    public void releaseRunwayOccupiedBy(AircraftColleague aircraftColleague) {
        if(aircraftRunwayMap.containsKey(aircraftColleague)){
            Runway runway = aircraftRunwayMap.remove(aircraftColleague);
            availableRunways.add(runway);
        }
    }

}
