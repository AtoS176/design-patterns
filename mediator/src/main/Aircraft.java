package main;

public class Aircraft implements AircraftColleague {
    private AircraftMediator mediator;
    private String flightName;

    public Aircraft(AircraftMediator mediator, String flightName) {
        this.mediator = mediator;
        this.flightName = flightName;
    }

    @Override
    public void startLanding() {
        String runway = mediator.allotRunwayTo(this);
        if(runway == null){
            System.out.println("Due to traffic, there's a delay in landing of" + flightName);
        }else{
            System.out.println("Currently landing " + flightName + " on " + runway);
        }
    }

    @Override
    public void finishLanding() {
        System.out.println(flightName + " landed successfully");
        mediator.releaseRunwayOccupiedBy(this);
    }

}
