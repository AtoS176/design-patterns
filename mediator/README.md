<h1>Wzorzec Mediator</h1>

<p>
Mediator to behawioralny wzorzec projektowy pozwalający zredukować
 chaos zależności pomiędzy obiektami. Wzorzec ten ogranicza bezpośrednią
  komunikację pomiędzy obiektami i zmusza je do współpracy wyłącznie za pośrednictwem obiektu mediatora
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li>Gdy chcemy uprościć komunikację między wieloma obiektami</li>
<li>Gdy chcemy zahermetyzować klasy, tak żeby nie wiedziały o sobie tylko pobierały zalezności przez mediatora.</li>
</ul>

<br>
<p>Linki :
<br>
<a href="http://www.programmergirl.com/mediator-design-pattern-java/">ProgrammerGirl</a>
<br>
<a href="https://javadevcentral.com/mediator-design-pattern">JavaDevCentral</a>
</p> 
