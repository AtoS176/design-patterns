<h1>Wzorzec Obserwator</h1>

<p>
Wzorzec projektowy obserwatora umożliwia subskrybentowi 
zarejestrowanie się w usłudze i otrzymywanie powiadomień od dostawcy. 
Jest to odpowiednie dla każdego scenariusza wymagającego powiadomień 
wypychanych. Wzorzec definiuje dostawcę (znanego również jako temat lub 
zauważalny) i zero, jeden lub więcej obserwatorów.
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li>Gdy zmiany stanu jednego obiektu mogą wymagać zmiany w innych obiektach</li>
<li>Gdy jakieś obiekty w Twojej aplikacji muszą obserwować inne, ale tylko przez jakiś czas 
lub w niektórych przypadkach</li>
</ul>

<br>
<p>Linki :
<br>
<a href="https://stackabuse.com/the-observer-design-pattern-in-java/">Stack Abuse</a>
<br>
</p> 