package test;

import main.*;

public class Runner {
    public static void main(String [] args){
        //Initialize
        Subject fashionStore = new Store();
        Observer customer1 = new PassiveCustomer();
        Observer customer2 = new ShopaholicCustomer();
        Observer customer3 = new ShopaholicCustomer();

        //Add two customers
        fashionStore.addSubscriber(customer1);
        fashionStore.addSubscriber(customer2);

        //Notify customers
        fashionStore.notifySubscriber();

        //A customer has decided not to continue following the newsletter
        fashionStore.removeSubscriber(customer1);

        //Add new customer
        fashionStore.addSubscriber(customer3);

        //Notify customers
        fashionStore.notifySubscriber();
    }
}
