package main;

import java.util.ArrayList;
import java.util.List;

public class Store implements Subject {
    private List<Observer> customers = new ArrayList<>();

    @Override
    public void addSubscriber(Observer observer) {
        customers.add(observer);
    }

    @Override
    public void removeSubscriber(Observer observer) {
        customers.remove(observer);
    }

    @Override
    public void notifySubscriber() {
        System.out.println("A new item is on sale! Act fast before it sells out");
        customers.forEach(customer -> customer.update("Sale !"));
    }
}
