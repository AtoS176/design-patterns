import main.Bugatti;
import main.Movable;
import main.MovableAdapter;
import main.MovableAdapterImpl;
import org.junit.Assert;
import org.junit.Test;

public class AdapterTest {
    @Test
    public void testAdapter(){
        Movable bugatti = new Bugatti();
        MovableAdapter bugattiAdapter = new MovableAdapterImpl(bugatti);

        Assert.assertEquals(bugattiAdapter.getSpeed(), 431.3, 0.1);
    }
}
