package main;

public interface MovableAdapter {
    double getSpeed();
}
