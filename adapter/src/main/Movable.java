package main;

public interface Movable {
    double getSpeed();
}
