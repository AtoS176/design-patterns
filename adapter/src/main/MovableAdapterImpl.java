package main;

public class MovableAdapterImpl implements MovableAdapter {
    private Movable luxuryCar;

    public MovableAdapterImpl(Movable luxuryCar) {
        this.luxuryCar = luxuryCar;
    }

    @Override
    public double getSpeed() {
        return this.converterMPHtoKMPH(luxuryCar.getSpeed());
    }

    private double converterMPHtoKMPH(double mph){
        return mph * 1.60934;
    }
}
