<h1>Wzorzec Adapter</h1>

<p>
Adapter to strukturalny wzorzec projektowy, który pozwala
na współdziałanie ze sobą obiektów o niekompatybilnych interfejsach.  
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li>Gdy chcemy wykorzystać istniejąca klasę, 
ale jej interfejs nie jest kompatybliny z resztą programu.</li>
<li>Gdy chcemy wykorzystać ponownie wiele istniejących podklas, którym brakuje jakieś wspólnej funkcjonalności.</li>
</ul>

<br>
<p>Linki :
<br>
<a href="https://refactoring.guru/pl/design-patterns/adapter">Refactoring Guru</a>
<br>
<a href="https://www.baeldung.com/java-adapter-pattern">Beldung</a>
</p> 


