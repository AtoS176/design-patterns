package test;

import main.abstractFactory.Hero;
import main.abstractFactory.equipment.ArcherEquipmentFactory;
import main.abstractFactory.equipment.MageEquipmentFactory;
import main.abstractFactory.equipment.WarriorEquipmentFactory;

public class Runner {
    public static void main (String [] args){
        Hero archer = new Hero("Archer", new ArcherEquipmentFactory());
        Hero mage = new Hero("Mage", new MageEquipmentFactory());
        Hero warrior = new Hero("Warrior", new WarriorEquipmentFactory());

        System.out.println(archer.toString());
        System.out.println(mage.toString());
        System.out.println(warrior.toString());

    }
}
