package main.weapon;

public class Staff extends Weapon {
    public Staff(String name, int attackValue, int level) {
        super(name, attackValue, level);
    }

    @Override
    public String describe() {
        return "Tylko dla magów." +  "\n" +
                " Całkowita siła ataku : " + attackValue + "\n";
    }

}
