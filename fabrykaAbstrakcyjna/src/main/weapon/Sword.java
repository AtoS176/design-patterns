package main.weapon;

public class Sword extends Weapon {
    public Sword(String name, int attackValue, int level) {
        super(name, attackValue, level);
    }

    @Override
    public String describe() {
        return "Tylko dla wojowników" + "\n" +
                "Wartość ataku : " + attackValue + "\n";
    }

}
