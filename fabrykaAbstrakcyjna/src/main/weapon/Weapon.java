package main.weapon;

public abstract class Weapon {
    protected final String name;
    protected final int attackValue;
    protected int level;

    public Weapon(String name, int attackValue, int level) {
        this.name = name;
        this.attackValue = attackValue;
        this.level = level;
    }

    public abstract String describe();
}
