package main.weapon;

public class Bow extends Weapon {
    public Bow(String name, int attackValue, int level) {
        super(name, attackValue, level);
    }

    @Override
    public String describe() {
        return "Tylko dla łuczników." + "\n" +
                "Wartość ataku : " + attackValue + "\n";
    }
}
