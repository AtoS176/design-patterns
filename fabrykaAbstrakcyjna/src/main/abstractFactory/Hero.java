package main.abstractFactory;

import main.abstractFactory.equipment.EquipmentFactory;
import main.armor.Armor;
import main.weapon.Weapon;

public class Hero {
    private final String nick;
    private final Weapon weapon;
    private final Armor armor;

    public Hero(String nick, EquipmentFactory equipmentFactory) {
        this.nick = nick;
        this.armor = equipmentFactory.careArmor();
        this.weapon = equipmentFactory.createWeapon();
    }

    @Override
    public String toString() {
        return "Gracz o nicku : " + nick + "\n" +
                "Posiada broń : " + "\n" + weapon.describe() + "\n" +
                "Posiada pancerz : " + "\n" + armor.describe() + "\n";
    }
}
