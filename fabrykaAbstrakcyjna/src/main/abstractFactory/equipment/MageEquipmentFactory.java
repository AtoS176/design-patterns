package main.abstractFactory.equipment;

import main.abstractFactory.equipment.EquipmentFactory;
import main.armor.Armor;
import main.armor.LightArmor;
import main.weapon.Staff;
import main.weapon.Weapon;

public class MageEquipmentFactory implements EquipmentFactory {
    @Override
    public Weapon createWeapon() {
        return new Staff("Staff", 80, 12);
    }

    @Override
    public Armor careArmor() {
        return new LightArmor(60, 320);
    }

}
