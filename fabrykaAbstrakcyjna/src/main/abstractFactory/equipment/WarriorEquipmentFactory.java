package main.abstractFactory.equipment;

import main.abstractFactory.equipment.EquipmentFactory;
import main.armor.Armor;
import main.armor.HeavyArmor;
import main.weapon.Sword;
import main.weapon.Weapon;

public class WarriorEquipmentFactory implements EquipmentFactory {
    @Override
    public Weapon createWeapon() {
        return new Sword("Sword", 140, 11);
    }

    @Override
    public Armor careArmor() {
        return new HeavyArmor(140, 520);
    }

}
