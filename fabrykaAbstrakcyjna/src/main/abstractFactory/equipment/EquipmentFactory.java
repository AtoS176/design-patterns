package main.abstractFactory.equipment;

import main.armor.Armor;
import main.weapon.Weapon;

public interface EquipmentFactory {
    Weapon createWeapon();
    Armor careArmor();
}
