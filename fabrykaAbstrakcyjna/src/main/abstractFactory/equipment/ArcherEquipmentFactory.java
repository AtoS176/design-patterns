package main.abstractFactory.equipment;

import main.armor.Armor;
import main.armor.NormalArmor;
import main.weapon.Bow;
import main.weapon.Weapon;

public class ArcherEquipmentFactory implements EquipmentFactory {
    @Override
    public Weapon createWeapon() {
        return new Bow("Bow", 400, 10);
    }

    @Override
    public Armor careArmor() {
        return new NormalArmor(70, 400);
    }

}
