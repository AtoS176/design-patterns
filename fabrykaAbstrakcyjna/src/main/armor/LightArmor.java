package main.armor;

public class LightArmor extends Armor {
    public LightArmor(int defense, int hp) {
        super(defense, hp);
    }

    @Override
    public String describe() {
        return "Tylko dla magów." + "\n" +
                "Obrona : " + defense + "\n" +
                "HP : " + hp + "\n";
    }

}
