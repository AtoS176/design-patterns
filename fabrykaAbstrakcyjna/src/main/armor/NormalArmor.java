package main.armor;

public class NormalArmor extends Armor {
    public NormalArmor(int defense, int hp) {
        super(defense, hp);
    }

    @Override
    public String describe() {
        return "Tylko dla łuczników." + "\n" +
                "Obrona : " + defense + "\n" +
                "HP : " + hp + "\n";
    }
}
