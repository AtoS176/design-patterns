package main.armor;

public class HeavyArmor extends Armor {
    public HeavyArmor(int defense, int hp) {
        super(defense, hp);
    }

    @Override
    public String describe() {
        return "Tylko dla wojowników." + "\n" +
                "Obrona : " + defense + "\n" +
                "HP : " + hp + "\n" ;
    }
}
