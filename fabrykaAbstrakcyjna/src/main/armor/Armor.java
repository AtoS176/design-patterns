package main.armor;

public abstract class Armor {
    protected int defense;
    protected int hp;

    public Armor(int defense, int hp) {
        this.defense = defense;
        this.hp = hp;
    }

    public abstract String describe();

}
