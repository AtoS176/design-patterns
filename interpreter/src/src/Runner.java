package src;

import main.AndExpression;
import main.Expression;
import main.TerminalExpression;

public class Runner {

    public static void main(String[] args) {
        Expression lions = new TerminalExpression("Lions");
        Expression tigers = new TerminalExpression("Tigers");
        String context = "Lions live in Africa";
        System.out.println(lions.interpret(context));

        Expression lionsAndTigers = new AndExpression(lions, tigers);
        System.out.println(lionsAndTigers.interpret(context));
    }

}
