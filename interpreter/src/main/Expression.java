package main;

public interface Expression {
    boolean interpret(String context);
}
