import java.util.Stack;

public class HeroCreator {
    private final Hero hero = new Hero();
    private final Stack<HeroMemento> mementos = new Stack<>();

    public void setType(Type type) {
        mementos.push(hero.backup());
        hero.setType(type);
    }

    public void setStrength(int strength) {
        mementos.push(hero.backup());
        hero.setStrength(strength);
    }

    public void setDefense(int defense) {
        mementos.push(hero.backup());
        hero.setDefense(defense);
    }

    public void setLuck(int luck) {
        mementos.push(hero.backup());
        hero.setLuck(luck);
    }

    public void undo(){
        hero.restore(mementos.pop());
    }

    public Hero getHero() {
        return hero;
    }

}
