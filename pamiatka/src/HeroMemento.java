public class HeroMemento {
    final Type type;
    final int strength;
    final int defense;
    final int luck;

    public HeroMemento(Type type, int strength, int defense, int luck) {
        this.type = type;
        this.strength = strength;
        this.defense = defense;
        this.luck = luck;
    }

}
