public class Hero {
    private Type type;
    private int strength;
    private int defense;
    private int luck;

    public void setType(Type type) {
        this.type = type;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public void setLuck(int luck) {
        this.luck = luck;
    }

    public HeroMemento backup(){
        return new HeroMemento(type, strength, defense, luck);
    }

    public void restore(HeroMemento memento){
        type = memento.type;
        strength = memento.strength;
        defense = memento.defense;
        luck = memento.luck;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "type=" + type +
                ", strength=" + strength +
                ", defense=" + defense +
                ", luck=" + luck +
                '}';
    }

}
