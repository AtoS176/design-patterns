public class Main {

    public static void main(String[] args) {
        HeroCreator heroCreator = new HeroCreator();
        heroCreator.setDefense(100);
        heroCreator.setLuck(92);
        heroCreator.setStrength(66);
        heroCreator.setType(Type.WARRIOR);
        heroCreator.setType(Type.ARCHER);
        heroCreator.setType(Type.WIZARD);

        Hero hero = heroCreator.getHero();
        System.out.println(hero);

        heroCreator.undo();
        System.out.println(hero);

        heroCreator.undo();
        System.out.println(hero);
    }

}
