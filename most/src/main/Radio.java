package main;

public class Radio implements Device {
    private int volume;
    private int channel;
    private boolean enable;

    public Radio(int volume, int channel){
        this.volume = volume;
        this.channel = channel;
        this.enable = false;
    }

    @Override
    public boolean isEnabled() {
        return this.enable;
    }

    @Override
    public void enable() {
        System.out.println("Radio turned on.");
        this.enable = true;
    }

    @Override
    public void disable() {
        System.out.println("Radio turned off.");
        this.enable = false;
    }

    @Override
    public int getVolume() {
        return this.volume;
    }

    @Override
    public void setVolume(int value) {
        this.volume = value;
    }

    @Override
    public int getChannel() {
        return this.channel;
    }

    @Override
    public void setChannel(int channel) {
        this.channel = channel;
    }
}
