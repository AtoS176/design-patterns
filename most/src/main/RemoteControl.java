package main;

//Abstraction
public class RemoteControl {
    private Device device;

    public RemoteControl(Device device){
        this.device = device;
    }

    public void togglePower(){
        if(device.isEnabled()){
            device.disable();
        }else{
            device.enable();
        }
    }

    public void volumeDown(){
        this.device.setVolume(device.getVolume() - 10);
    }

    public void volumeUp(){
        this.device.setVolume(device.getVolume() + 10);
    }

    public void channelDown(){
        this.device.setChannel(device.getChannel() - 1);
    }

    public void channelUp(){
        this.device.setChannel(device.getChannel() + 1);
    }

}
