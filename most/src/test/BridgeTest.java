import main.Device;
import main.Radio;
import main.RemoteControl;
import main.Tv;
import org.junit.Assert;
import org.junit.Test;

public class BridgeTest {
    @Test
    public void tvTest(){
        //given
        Device tv = new Tv(10, 10);
        RemoteControl tvRemoteControl = new RemoteControl(tv);
        //when
        tvRemoteControl.channelDown();
        tv.enable();
        //then
        Assert.assertEquals(tv.getChannel(), 9);
        Assert.assertTrue(tv.isEnabled());
    }

    @Test
    public void radioTest(){
        //given
        Device radio = new Radio(5, 5);
        RemoteControl radioRemoteControl = new RemoteControl(radio);
        //when
        radioRemoteControl.volumeUp();
        radioRemoteControl.channelUp();
        //then
        Assert.assertEquals(radio.getChannel(), 6);
        Assert.assertEquals(radio.getVolume(), 15);
    }
}
