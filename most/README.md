<h1>Wzorzec Most</h1>

<p>Most to strukturalny wzorzec projektowy pozwalający na rozdzielenie dużej klasy
lub zestawu spokrewnionych klas na dwie hierarchie : abstrakcję oraz implementację.
Dzięki temu nad obiema można wówczas pracować niezależnie.</p>

<h4>Kiedy stosować : </h4>

<ul>
<li>Gdy chcę rozdzielić i przeorganizować <b>monolityczną</b> klasę, 
która posiada wiele wariantów takiej samej funkcjonalności.</li>
<li>Rozszerzenie klasy na kilku niezależnych płaszczyznach.</li>
<li>Spełnia wymóg możliwości wyboru implementacji w trakcie działania programu.</li>
</ul>

<br>
<p>Linki :
<br>
<a href="https://refactoring.guru/pl/design-patterns/bridge">Refactoring Guru</a>
</p>
