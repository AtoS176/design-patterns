<h1>Wzorzec Budowniczy</h1>

<p>
Budowniczy jest kreacyjnym wzorcem projektowym, który daje możliwość tworzenia złożonych
 obiektów etapami, krok po kroku. Wzorzec ten pozwala produkować różne typy 
 oraz reprezentacje obiektu używając tego samego kodu konstrukcyjnego.
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li>Stosuj wzorzec Budowniczy, aby pozbyć się “teleskopowych konstruktorów”.</li>
<li>Stosuj wzorzec Budowniczy, gdy potrzebujesz możliwości tworzenia różnych reprezentacji jakiegoś produktu (na przykład, domy z kamienia i domy z drewna).</li>
</ul>


<br>
<p>Linki :
<br>
<a href="https://java-design-patterns.com/patterns/builder/">Java Design Pattern</a>
<br>
<a href="https://www.stackabuse.com/the-builder-design-pattern-in-java/">Stack Abuse</a>
<br>
<a href="https://springframework.guru/gang-of-four-design-patterns/builder-pattern/">Spring Framework Guru</a>
<br>
<a href="https://bulldogjob.pl/news/348-idiotoodporne-api-klasy-czyli-jakie">Bulldog Job</a>
</p> 