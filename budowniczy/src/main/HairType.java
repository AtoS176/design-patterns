package main;

public enum  HairType {
    BALD,
    CURLY,
    LONG_CURLY,
    LONG_STRAIGHT,
    SHORT
}
