package main;

public enum HairColor {
    BLACK,
    BLOND,
    BROWN,
    RED,
    WHITE
}
