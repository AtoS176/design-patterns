package main;

public enum Weapon {
    AXE,
    BOW,
    DAGGER,
    SWORD,
    STAFF
}
