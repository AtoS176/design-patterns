package test;

import main.*;

public class Main {
    public static void main (String [] args){
        Hero mage = new Hero.Builder(Profession.MAGE, "Razor")
                .withArmor(Armor.CLOTHES)
                .withHairType(HairType.LONG_CURLY)
                .withHairColor(HairColor.BLOND)
                .withWeapon(Weapon.STAFF)
                .build();

        System.out.println(mage.toString());

        Hero archer = new Hero.Builder(Profession.ARCHER, "Jonson")
                .withArmor(Armor.LEATHER)
                .withWeapon(Weapon.BOW)
                .build();

        System.out.println(archer.toString());

    }
}
