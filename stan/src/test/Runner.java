package test;

import main.Car;
import main.FirstGear;

public class Runner {
    public static void main(String [] args){
        Car car = new Car();
        car.accelerate(40);

        //First Gear
        car.changeState(new FirstGear());
        car.accelerate(30);

        // Try to reduce
        car.reduceGear();

        // Second Gear
        car.increaseGear();
        car.accelerate(90);
        car.accelerate(60);

        // Third Gear
        car.increaseGear();
        car.accelerate(100);

        //Reduce to Second Gear
        car.reduceGear();
        car.accelerate(50);
    }
}
