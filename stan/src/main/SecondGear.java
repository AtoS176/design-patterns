package main;

public class SecondGear implements GearState {
    private final int MAX_SPEED = 80;

    @Override
    public void setNext(Car car) {
        car.changeState(new ThirdGear());
    }

    @Override
    public void setPrevious(Car car) {
        car.changeState(new FirstGear());
    }

    @Override
    public void accelerate(int speed) {
        if(speed > MAX_SPEED){
            System.out.println("Can not be accelerate to " + speed + " kmp in gear 2");
        }else{
            System.out.println("Car is running " + speed + " kmp in second gear");
        }
    }

}
