package main;

public interface GearState {
    void setNext(Car car);
    void setPrevious(Car car);
    void accelerate(int speed);
}
