package main;

public class Car {
    private GearState gearState;

    public void increaseGear() {
        gearState.setNext(this);
    }

    public void reduceGear() {
        gearState.setPrevious(this);
    }

    public void accelerate(int speed) {
        if(gearState == null){
            System.out.println("Car cannot be accelerated as its in Neutral");
        }else{
            gearState.accelerate(speed);
        }
    }

    public void changeState(GearState gearState){
        this.gearState = gearState;
    }

}
