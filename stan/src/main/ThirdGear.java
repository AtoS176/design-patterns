package main;

public class ThirdGear implements GearState {
    private final int MAX_SPEED = 110;

    @Override
    public void setNext(Car car) {
        System.out.println("Increase gear is impossible");
    }

    @Override
    public void setPrevious(Car car) {
        car.changeState(new SecondGear());
    }

    @Override
    public void accelerate(int speed) {
        if(speed > MAX_SPEED){
            System.out.println("Can not be accelerate to " + speed + " kmp in gear 3");
        }else{
            System.out.println("Car is running " + speed + " kmp in third gear");
        }
    }
}
