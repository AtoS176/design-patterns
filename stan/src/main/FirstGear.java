package main;

public class FirstGear implements GearState {
    private final int MAX_SPEED = 40;

    @Override
    public void setNext(Car car) {
        car.changeState(new SecondGear());
    }

    @Override
    public void setPrevious(Car car) {
        System.out.println("Reduction is impossible");
    }

    @Override
    public void accelerate(int speed) {
        if(speed > MAX_SPEED){
            System.out.println("Can not be accelerate to " + speed + " kmp in gear 1");
        }else{
            System.out.println("Car is running " + speed + " kmp in first gear");
        }
    }

}
