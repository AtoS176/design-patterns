<h1>Wzorzec Stan</h1>

<p>
Stan to behawioralny wzorzec projektowy pozwalający obiektowi zmienić swoje zachowanie gdy zmieni 
się jego stan wewnętrzny. Wygląda to tak, jakby obiekt zmienił swoją klasę.
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li>Gdy masz do czynienia z obiektem którego zachowanie jest zależne od jego stanu, 
liczba możliwych stanów jest wielka, a kod specyficzny dla 
danego stanu często ulega zmianom.</li>
<li> Gdy masz klasę zaśmieconą rozbudowanymi instrukcjami warunkowymi zmieniającymi zachowanie klasy zależnie od wartości jej pól.</li>
</ul>

<br>
<p>Linki :
<br>
<a href="https://refactoring.guru/pl/design-patterns/state">Refactoring Guru</a>
<br>
<a href="https://www.baeldung.com/java-state-design-pattern">Beldung</a>
</p> 