<h1>Wzorzec Odwiedzający</h1>

<p>
Odwiedzający to czynnościowy wzorzec projektowy pozwalający oddzielić algorytmy od obiektów na których pracują.
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li>Stosuj wzorzec Odwiedzający gdy istnieje potrzeba wykonywania jakiegoś działania na wszystkich elementach złożonej struktury obiektów (jak drzewo obiektów).</li>
<li>Stosowanie Odwiedzającego pozwala uprzątnąć logikę biznesową czynności pomocniczych.</li>
</ul>
<br>
<p>Linki :
<br>
<a href="https://refactoring.guru/pl/design-patterns/visitor">Refactoring Guru</a>
<br>
<a href="https://howtodoinjava.com/design-patterns/behavioral/visitor-design-pattern-example-tutorial/">HowToDoInJava</a>
</p> 