import router.CiscoRouter;
import router.LinkSysRouter;
import router.NetgearRouter;
import router.Router;
import visitor.LinuxConfig;
import visitor.RouterVisitor;
import visitor.WindowConfig;

public class VisitorTest {
    public static void main(String[] args) {
        Router cisco = new CiscoRouter();
        Router linksys = new LinkSysRouter();
        Router netgear = new NetgearRouter();

        RouterVisitor linux = new LinuxConfig();
        RouterVisitor windows = new WindowConfig();

        cisco.accept(linux);
        cisco.accept(windows);

        linksys.accept(linux);
        linksys.accept(windows);

        netgear.accept(linux);
        netgear.accept(windows);
    }
}
