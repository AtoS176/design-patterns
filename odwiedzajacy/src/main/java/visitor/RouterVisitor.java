package visitor;

import router.CiscoRouter;
import router.LinkSysRouter;
import router.NetgearRouter;

public interface RouterVisitor {
    void visit(CiscoRouter router);
    void visit(LinkSysRouter router);
    void visit(NetgearRouter router);
}
