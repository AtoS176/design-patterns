package visitor;

import router.CiscoRouter;
import router.LinkSysRouter;
import router.NetgearRouter;

public class LinuxConfig implements RouterVisitor {
    @Override
    public void visit(CiscoRouter router) {
        System.out.println("Cisco router config complete on Linux!");
    }

    @Override
    public void visit(LinkSysRouter router) {
        System.out.println("LinkSys router config complete on Linux!");
    }

    @Override
    public void visit(NetgearRouter router) {
        System.out.println("Netgear router config complete on Linux!");
    }
}
