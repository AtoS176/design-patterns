package router;

import visitor.RouterVisitor;

public interface Router {
    void sendData(byte[] data);
    void acceptData(byte [] data);
    void accept(RouterVisitor visitor);
}
