package router;

import visitor.RouterVisitor;

public class NetgearRouter implements Router {
    @Override
    public void sendData(byte[] data) { }

    @Override
    public void acceptData(byte[] data) { }

    @Override
    public void accept(RouterVisitor visitor) {
        visitor.visit(this);
    }

}
