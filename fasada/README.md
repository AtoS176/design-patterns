<h1>Wzorzec Fasada</h1>

<p>
Fasada służy do ujednolicenia dostępu do złożonego systemu poprzez 
udostępnienie uproszczonego i uporządkowanego interfejsu programistycznego. 
Fasada zwykle implementowana jest w bardzo prosty sposób – w postaci jednej 
klasy powiązanej z klasami reprezentującymi system, do którego klient chce 
uzyskać dostęp. 
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li>Użyj wzorca Fasada gdy potrzebujesz ograniczonego, ale łatwego w użyciu interfejsu do złożonego podsystemu.</li>
<li> Stosuj Fasadę gdy chcesz ustrukturyzować podsystem w warstwy.</li>
</ul>

<br>
<p>Linki :
<br>
<a href="https://springframework.guru/gang-of-four-design-patterns/facade-pattern/">Spring Guru Framework</a>
<br>
<a href="https://refactoring.guru/design-patterns/facade">Refactoring Guru</a>
</p> 