import main.OrderFulfillmentController;
import main.OrderServiceFacadeImpl;
import org.junit.Assert;
import org.junit.Test;

public class OrderFulfillmentControllerTest {

    @Test
    public void testOrderProduct() {
        OrderFulfillmentController controller = new OrderFulfillmentController(new OrderServiceFacadeImpl());
        controller.orderProduct(9);
        boolean result = controller.orderFulfilled;
        Assert.assertTrue(result);
    }

}
