package main;

public class OrderFulfillmentController {
    private final OrderServiceFacade facade;
    public boolean orderFulfilled = false;

    public OrderFulfillmentController(OrderServiceFacade facade) {
        this.facade = facade;
    }

    public void orderProduct(int productId){
        orderFulfilled=facade.placeOrder(productId);
        System.out.println("Order Completed");
    }
}
