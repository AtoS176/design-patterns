package main;

final public class VP extends Handler {

    @Override
    public void handleRequest(Request request) {
        if (Type.PURCHASE.equals(request.type()) && request.amount() < 1000) {
            System.out.println("Handled by VP");
        } else {
            successor.handleRequest(request);
        }
    }

}
