package main;

public class Director extends Handler {

    @Override
    public void handleRequest(Request request) {
        if (Type.CONFERENCE.equals(request.type())) {
            System.out.println("Handled by Director");
        } else {
            successor.handleRequest(request);
        }
    }

}
