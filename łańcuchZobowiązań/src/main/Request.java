package main;

public record Request(Type type, int amount) {
}
