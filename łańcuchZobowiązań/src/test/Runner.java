package test;

import main.*;

public class Runner {
    public static void main(String[] args) {
        Director adam = new Director();
        VP thom = new VP();
        CEO alex = new CEO();

        adam.setSuccessor(thom);
        thom.setSuccessor(alex);

        adam.handleRequest(new Request(Type.CONFERENCE, 100));
        adam.handleRequest(new Request(Type.PURCHASE, 100));
        adam.handleRequest(new Request(Type.PURCHASE, 2000));
    }
}
