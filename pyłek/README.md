<h1>Wzorzec Pyłek</h1>

<p>
Pyłek to strukturalny wzorzec projektowy, którego celem jest zmniejszenie wykorzystania
pamięci RAM. W obiekcie dokonujemy podziału danych na dane wewnętrzne (współdzielone)
oraz zewnętrzne (unikatowe dla każdego obiektu). Dokonujemy podziału naszego obiektu 
na dwie klasy zawierające :

 <ul>
 <li> stan wewnętrzny : Pyłek </li>
 <li> stan zewnętrzny oraz referencję do konkretnej instancji klasy Pyłek : kontekst</li>
 </ul>


<h4>Kiedy stosować : </h4>
<ul>
<li>Gdy aplikacja musi pracować z wielką ilością obiektów i mamy ograniczoną pamieć RAM.</li>

</ul>

<br>
<p>Linki :
<br>
<a href="https://refactoring.guru/design-patterns/flyweight">Refactoring Guru</a>
<br>
<a href="https://howtodoinjava.com/design-patterns/structural/flyweight-design-pattern/">HowToDoInJava</a>
</p> 