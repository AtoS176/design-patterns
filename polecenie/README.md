<h1>Wzorzec Polecenie</h1>

<p>
Polecenie to czynnościowy wzorzec projektowy, traktujący żądanie wykonania
 określonej czynności jako obiekt, dzięki czemu mogą być one
  parametryzowane w zależności od rodzaju odbiorcy, a także umieszczane w
   kolejkach i dziennikach. 
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li>Gdy chcesz parametryzować obiekty za pomocą działań.</li>
<li>Wzorzec Polecenie pozwala układać kolejki zadań, ustalać harmonogram ich wykonania bądź uruchamiać je zdalnie.</li>
<li>Stosuj wzorzec Polecenie gdy chcesz zaimplementować operacje odwracalne.</li>
</ul>

<br>
<p>Linki :
<br>
<a href="https://refactoring.guru/pl/design-patterns/command">Refactoring Guru</a>
<br>
<a href="https://dzone.com/articles/design-patterns-command">DZone</a>
</p> 