package test;

import main.Circle;
import main.Factory;

import java.util.stream.IntStream;

public class Runner {

    public static void main(String[] args) {
        Factory<Circle> blueCircleFactory = Factory.createFactory(Circle::new, "BLUE");
        Factory<Circle> singletonFactory = Factory.createSingleton(Circle::new);

        IntStream.range(0, 2)
                .mapToObj(id -> blueCircleFactory.newInstance())
                .forEach(Runner::print);

        System.out.println();

        IntStream.range(0, 2)
                .mapToObj(id -> singletonFactory.newInstance())
                .forEach(Runner::print);
    }

    public static void print(Circle circle){
        System.out.println(circle.toString() + " " + circle.hashCode());
    }

}
