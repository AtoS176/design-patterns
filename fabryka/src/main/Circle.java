package main;

public class Circle {
    private final String color;

    public Circle() {
        this.color = "WHITE";
    }

    public Circle(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "color='" + color + '\'' +
                '}';
    }

}
