package main.product;

public interface EncryptionAlgorithm {
    String encrypt(String text);
}
