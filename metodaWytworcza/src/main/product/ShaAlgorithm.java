package main.product;

import org.apache.commons.codec.digest.DigestUtils;

public class ShaAlgorithm implements EncryptionAlgorithm {
    @Override
    public String encrypt(String text) {
        return DigestUtils.shaHex(text);
    };
}
