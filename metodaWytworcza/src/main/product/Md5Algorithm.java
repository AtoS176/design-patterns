package main.product;

import org.apache.commons.codec.digest.DigestUtils;

public class Md5Algorithm implements EncryptionAlgorithm {
    @Override
    public String encrypt(String text) {
        return DigestUtils.md5Hex(text);
    }
}
