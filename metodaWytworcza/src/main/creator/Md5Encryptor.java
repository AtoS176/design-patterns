package main.creator;

import main.product.EncryptionAlgorithm;
import main.product.Md5Algorithm;

public class Md5Encryptor extends Encryptor {
    @Override
    public EncryptionAlgorithm factoryMethod() {
        return new Md5Algorithm();
    }
}
