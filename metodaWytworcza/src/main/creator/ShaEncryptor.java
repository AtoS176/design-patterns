package main.creator;

import main.product.EncryptionAlgorithm;
import main.product.ShaAlgorithm;

public class ShaEncryptor extends Encryptor {
    @Override
    public EncryptionAlgorithm factoryMethod() {
        return new ShaAlgorithm();
    }
}
