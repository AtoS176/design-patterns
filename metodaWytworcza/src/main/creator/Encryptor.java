package main.creator;

import main.product.EncryptionAlgorithm;

import java.io.FileOutputStream;
import java.io.IOException;

public abstract class Encryptor {
    public void writeToDisk(String text, String filename){
        EncryptionAlgorithm encryptionAlgorithm = factoryMethod();
        String encrypted = encryptionAlgorithm.encrypt(text);

        try {
            FileOutputStream outputStream = new FileOutputStream(filename);
            outputStream.write(encrypted.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public abstract EncryptionAlgorithm factoryMethod();

}
