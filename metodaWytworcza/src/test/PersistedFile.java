package test;

import main.creator.Encryptor;

public class PersistedFile {
    private final String path;
    private final String text;
    private final Encryptor encryptor;

    public PersistedFile(String path, String text, Encryptor encryptor) {
        this.path = path;
        this.text = text;
        this.encryptor = encryptor;
    }

    public void persist(){
        encryptor.writeToDisk(text, path);
    }

}
