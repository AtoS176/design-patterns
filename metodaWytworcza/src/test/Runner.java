package test;

import main.creator.Md5Encryptor;
import main.creator.ShaEncryptor;

public class Runner {
    public static void main (String [] args){
        PersistedFile md5Encrypted = new PersistedFile("md5.text", "Factory Method", new Md5Encryptor());
        md5Encrypted.persist();

        PersistedFile shaEncrypted = new PersistedFile("sha.text", "Factory Method", new ShaEncryptor());
        shaEncrypted.persist();
    }
}
