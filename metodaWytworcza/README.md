<h1>Wzorzec Metoda Wytwórcza</h1>

<p>
Metoda wytwórcza jest kreacyjnym wzorcem projektowym, który udostępnia interfejs
do tworzenia obiektów w ramach klasy bazowej, ale pozwala podklasom zmieniać typ 
tworzonych obiektów.
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li>Gdy nie wiesz z góry jakie typy obiektów pojawią się w twoim programie i jakie będą między nimi zależności.</li>
<li>Gdy chcesz umożliwić osbobą korzystającym z twojej bilbioteki na rozbudowanie jej wewnętrznych komponentów</li>
<li>Gdy chcesz oszczędniej wykorzystać zasoby systemowe poprzez ponowne wykorzystanie już istniejących obiektów, zamiast odbudowywać je raz za razem.</li>
</ul>


<br>
<p>Linki :
<br>
<a href="https://refactoring.guru/pl/design-patterns/factory-method">Refactoring Guru</a>
<br>
<a href="https://dzone.com/articles/java-the-factory-pattern">Dzone</a>
</p> 