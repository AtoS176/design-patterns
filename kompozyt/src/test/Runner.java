package test;

import main.Circle;
import main.CompoundShape;
import main.Dot;
import main.Rectangle;

import java.awt.*;

public class Runner {
    public static void main(String[] args){
        ImageEditor imageEditor = new ImageEditor();

        imageEditor.loadShape(
                new Circle(10, 10, 10, Color.PINK),

                new CompoundShape(
                        new Circle(110, 110, 50, Color.RED),
                        new Dot(160, 160, Color.RED)
                ),

                new CompoundShape(
                        new Dot(240, 240, Color.GREEN),
                        new Dot(240, 360, Color.GREEN),
                        new Dot(360, 360, Color.GREEN),
                        new Dot(360, 240, Color.GREEN),
                        new Rectangle(250, 250, 100, 100, Color.GREEN)
                )

        );

    }
}
