<h1>Wzorzec Kompozyt</h1>

<p>
Kompozyt to strukturalny wzorzec projektowy pozwalający komponować obiekty w struktury drzewiaste, a następnie traktować te struktury jakby były osobnymi obiektami.
</p>

<h4>Kiedy stosować : </h4>
<ul>
<li>Gdy musimy zaimplementować drzewiastą strukturę obiektów.</li>
<li>Gdy chcemy aby kod kliencki traktował zarówno proste, jak i złożone elementy w jednakowy sposób.</li>
</ul>
<br>
<p>Linki :
<br>
<a href="https://refactoring.guru/pl/design-patterns/composite">Refactoring Guru</a>
<br>
<a href="https://www.baeldung.com/java-composite-pattern">Beldung</a>
<br>
<a href="https://dzone.com/articles/composite-design-pattern-java-0">Dzone</a>
</p> 